const path = require("path");
const express = require("express");
const createError = require("http-errors");
const config = require("./config");
const app = express();
const passport = require("passport");
const authenticate = require("./authenticate");

// routes
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/userRouter");
const dishRouter = require("./routes/dishRouter");
const promoRouter = require("./routes/promoRouter");
const leaderRouter = require("./routes/leaderRouter");
const favoriteRouter = require("./routes/favoriteRouter");

// db
const mongoose = require("mongoose");
const url = config.mongoUrl;
const connect = mongoose.connect(url);
connect.then(
  (db) => {
    console.log("Connected correctly to server");
  },
  (err) => {
    console.log(err);
  }
);

// const session = require("express-session");
// const FileStore = require("session-file-store")(session);

// const cookieParser = require("cookie-parser");
// app.use(cookieParser());
// view engine setup
app.use(passport.initialize());
// app.use(passport.session());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, "public")));

// routes
app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/favorites", favoriteRouter);
app.use("/dishes", dishRouter);
app.use("/promotions", promoRouter);
app.use("/leaders", leaderRouter);


function auth(req, res, next) {
  console.log(req.user);
  if (!req.user) {
    var err = new Error("You are not authenticated!");
    err.status = 403;
    next(err);
  } else {
    next();
  }
}
app.use(auth);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
