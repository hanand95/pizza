const express = require("express");
const mongoose = require("mongoose");
const authenticate = require("../authenticate");
const Favorites = require("../models/favorite");
const favoriteRouter = express.Router();

favoriteRouter.use(express.urlencoded({ extended: true }));
favoriteRouter.use(express.json());

favoriteRouter
  .route("/")
  .get(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({})
      .populate("user")
      .populate("dishes")
      .then(
        (allResults) => {
          if (allResults) {
            favorites = allResults.filter(
              (fav) => fav.user._id.toString() === req.user.id.toString()
            )[0];
            if (!favorites) {
              var err = new Error("You have no favorites!");
              err.status = 404;
              return next(err);
            }
            res.statusCode = 200;
            res.setHeader("Content-Type", "application/json");
            res.json(favorites);
          } else {
            var err = new Error("There are no favorites");
            err.status = 404;
            return next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  })
  .post(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({})
      .populate("user")
      .populate("dishes")
      .then((allResults) => {
        var favorites;
        if (allResults)
          favorites = allResults.filter(
            (fav) => fav.user._id.toString() === req.user.id.toString()
          )[0];
        if (!user) favorites = new Favorites({ user: req.user.id });
        for (let i of req.body) {
          if (
            favorites.dishes.find((dish) => {
              if (dish._id) {
                return dish._id.toString() === i._id.toString();
              }
            })
          )
            continue;
          favorites.dishes.push(i._id);
        }
        favorites
          .save()
          .then(
            (result) => {
              res.statusCode = 201;
              res.setHeader("Content-Type", "application/json");
              res.json(result);
              console.log("Favorites Created");
            },
            (err) => next(err)
          )
          .catch((err) => next(err));
      })
      .catch((err) => next(err));
  })
  .put(authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end("PUT operation is not supported on /favorites");
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({})
      .populate("user")
      .populate("dishes")
      .then(
        (allResults) => {
          var favorites;
          if (allResults) {
            favorites = allResults.filter(
              (fav) => fav.user._id.toString() === req.user.id.toString()
            )[0];
          }
          if (favorites) {
            favorites.remove().then(
              (result) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(result);
              },
              (err) => next(err)
            );
          } else {
            var err = new Error("You do not have any favorites");
            err.status = 404;
            return next(err);
          }
        },
        (err) => next(err)
      )
      .catch((err) => next(err));
  });

favoriteRouter
  .route("/:dishId")
  .post(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({})
      .populate("user")
      .populate("dishes")
      .then((allResults) => {
        var favorites;
        if (allResults)
          favorites = allResults.filter(
            (fav) => fav.user._id.toString() === req.user.id.toString()
          )[0];
        if (!favorites) favorites = new Favorites({ user: req.user.id });
        if (
          !favorites.dishes.find((d_id) => {
            if (d_id._id)
              return d_id._id.toString() === req.params.dishId.toString();
          })
        )
          favorites.dishes.push(req.params.dishId);

        favorites
          .save()
          .then(
            (result) => {
              res.statusCode = 201;
              res.setHeader("Content-Type", "application/json");
              res.json(result);
              console.log("Favorites Created");
            },
            (err) => next(err)
          )
          .catch((err) => next(err));
      })
      .catch((err) => next(err));
  })
  .delete(authenticate.verifyUser, (req, res, next) => {
    Favorites.find({})
        .populate('user')
        .populate('dishes')
        .then((allResults) => {
            var favorites;
            if(allResults)
                favorites = allResults.filter(fav => fav.user._id.toString() === req.user.id.toString())[0];
            if(favorites){
                favorites.dishes = favorites.dishes.filter((dishid) => dishid._id.toString() !== req.params.dishId);
                favorites.save()
                    .then((result) => {
                        res.statusCode = 200;
                        res.setHeader("Content-Type", "application/json");
                        res.json(result);
                    }, (err) => next(err));
                
            } else {
                var err = new Error('You do not have any favorites');
                err.status = 404;
                return next(err);
            }
        }, (err) => next(err))
        .catch((err) => next(err));
  });

module.exports = favoriteRouter;
